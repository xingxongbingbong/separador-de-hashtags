# Separador de Hashtags

Separa 10 hashtags de modo aleatório para ajudar na relevância de redes sociais.

# Como usar
1. [Instale python3](https://www.python.org/downloads/) no seu computador (Verifique no instalador se ele foi adicionado ao PATH)
2. Edite o arquivo `Hashtags.txt`: Coloque todas as hashtags que desejar, utilizando sempre # e separando cada uma por um espaço. Coloque no mínimo 11.
    1.  **É importante** sempre clicar no final da última e apertar o `delete`, para que o programa não pegue um espaço em branco no final do texto.
    2. Você pode utilizar uma ferramenta como o [Bigbangram](https://bigbangram.com/instagram-promotion/hashtags/hashtag-generator/#) para selecionar as melhores hashtags.
3. Dê um duplo clique no programa `separarhashtags.py` e aguarde alguns segundos.
4. Abra o arquivo `Descricao.txt` e copie as hashtags.
