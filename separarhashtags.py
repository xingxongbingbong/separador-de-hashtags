import random

def main():
	AH=open('Hashtags.txt','r').read() #abrir o arquivo de hashtags e entender tudo como string
	LH=[] #lista principal com todas as hashtags
	LH=AH.split(' ') #insere as hashtags do arquivo na lista LH
	sLH=[] #lista auxiliar para contar itens e repeticoes
		
	Descricao=open('Descricao.txt','w')
		
	while len(sLH)<10: #conta o numero de itens na lista auxiliar 
		itemLH=random.choice(LH) #escolhe aleatoriamente na lista principal e coloca no itemLH
		if itemLH in sLH: #se o item escolhido tiver na lista de suporte, escolha novamente
			itemLH=random.choice(LH)
		else:
			sLH.append(itemLH) #se o item escolhido nao tiver na lista de suporte, coloque ele
			Descricao.write(itemLH) #escreva esse item no arquivo
			Descricao.write(" ") #colocar espaco
		
	Descricao.close()

main()
